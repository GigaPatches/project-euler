/**
 * Problem 5
 *
 * What is the smallest number that is evenly divisible by all of the numbers from 1 to 20?
 */

#include <iostream>

using std::cout;
using std::endl;

bool divisible_check(int n)
{
	for (int i = 1; i <= 20; ++i)
		if ((n % i) != 0)
			return false;
	return true;
}

int main()
{
	int i = 1;

	while (!divisible_check(i)) ++i;

	cout << "Result: " << i << endl;

	return 0;
}
