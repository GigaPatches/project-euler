/**
 * Problem 20
 *
 * Find the sum of the digits in the number 100!
 */

#include <iostream>
#include <gmpxx.h>

using namespace std;

mpz_class factorial(int n)
{
	int i = n;
	mpz_class v = 1;
	while (i > 0)
	{
		v *= i;
		--i;
	}

	return v;
}

int main()
{
	mpz_class n = factorial(100);
	string s = n.get_str();

	int sum = 0;
	for (int i = 0; i < s.length(); ++i)
		sum += static_cast<int>(s.at(i) - 48);
	cout << sum << endl;

	return 0;
}
