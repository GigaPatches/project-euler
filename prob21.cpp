/**
 * Problem 21
 *
 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called
 * amicable numbers.
 *
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
 * The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 *
 * Evaluate the sum of all the amicable numbers under 10000.
 */

#include <iostream>
#include <math.h>

using namespace std;

/**
 * Refined based on the code from Project Euler reference.
 * My code was no where as efficient, but was much easier.
 */
int d(int n)
{
	if (n <= 1)
		return 0;

	int r = floor(sqrt(n));
	int sum = 0;
	int i = 0;
	int f = 0;

	if ((r * r) == n)
	{
		sum = r + 1;
		r = r - 1;
	}
	else
		sum = 1;

	if ((n % 2) == 1)
	{
		f = 3;
		i = 2;
	}
	else
	{
		f = 2;
		i = 1;
	}


	while (f <= r)
	{
		if ((n % f) == 0)
			sum += f + n / f;
		f += i;
	}

	return sum;
}

int main()
{
	int t, result = 0;
	for (int i = 1; i < 10000; ++i)
	{
		t = d(i);
		if (i != t && i == d(t))
			result += i;
	}

	cout << result << endl;
	return 0;
}
