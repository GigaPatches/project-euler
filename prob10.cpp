/**
 * Problem 10
 *
 * Find the sum of all the primes below 2 million.
 */

#include <iostream>
#include "primes.h"

using namespace std;

typedef unsigned long long uint64;

int main()
{
	uint64 sum = 0;

	for (int i = 2; i < 2000000; ++i)
		if (is_prime(i))
			sum += i;

	cout << sum << endl;

	return 0;
}
