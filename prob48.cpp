/**
 * Problem 48
 *
 * The series, 1^(1) + 2^(2) + 3^(3) + ... + 10^(10) = 10405071317.
 *
 * Find the last ten digits of the series, 1^(1) + 2^(2) + 3^(3) + ... + 1000^(1000).
 */

#include <iostream>
#include <string>
#include <sstream>
#include <gmpxx.h>
#include <gmp.h>

using namespace std;

int main()
{
	int i;
	mpz_class result = 0, t = 0, t2;

	for (i = 1; i <= 1000; ++i)
	{
		mpz_ui_pow_ui(t.get_mpz_t(), i, i);
		result += t;
	}

	string num;
	stringstream s;
	s << result;
	s >> num;
	cout << num.substr(num.length() - 10, 10) << endl;

	return 0;
}
