/**
 * Problem 50
 *
 * The prime 41, can be written as the sum of six consecutive primes:
 * 41 = 2 + 3 + 5 + 7 + 11 + 13
 *
 * This is the longest sum of consecutive primes that adds to a prime below one-hundred.
 *
 * The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
 *
 * Which prime, below one-million, can be written as the sum of the most consecutive primes?
 */

#include <iostream>
#include <vector>
#include "primes.h"
#define MAX_NUMBER  999999

using namespace std;

/*bool primes[PRIME_COUNT];

int next_prime(int n)
{
	while (++n < PRIME_COUNT && !primes[n]);
	return n;
}*/

int main()
{
	vector<int> primes;
	vector<int>::iterator p, t;
	int n, maxc = 0, maxn = 0, c;

	for (int i = 0; i <= MAX_NUMBER; ++i)
		if (is_prime(i))
			primes.push_back(i);


	for (p = primes.begin(); p != primes.end(); ++p)
	{
		n = 0;
		c = 0;

		for (t = p; t != primes.end(); ++t)
		{
			n += *t;

			if (n > MAX_NUMBER)
			{
				n -= *t;
				break;
			}
			
			++c;

			if (is_prime(n))
			{
				if (c > maxc)
				{
					maxc = c;
					maxn = n;
				}
			}
		}
	}

	cout << maxn << ' ' << maxc << endl;


	return 0;
}
