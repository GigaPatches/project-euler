/**
 * Problem 37
 *
 * The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits
 * from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to 
 * left: 3797, 379, 37, and 3.
 *
 * Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
 */

#include <iostream>
#include "primes.h"

using namespace std;

int reverse(int n)
{
	int r = 0;

	while (n > 0)
	{
		r = (r * 10) + (n % 10);
		n /= 10;
	}

	return r;
}

int main()
{
	int sum = 0, found = 0;

	for (int i = 11; found < 11; ++i)
	{
		if (!is_prime(i))
			continue;

		int n = i;
		do
		{
			n /= 10;
		} while (is_prime(n) && n > 0);

		if (n != 0)
			continue;

		n = i;
		do
		{
			n = reverse(reverse(n) / 10);
		} while (is_prime(n) && n > 0);

		if (n == 0)
		{
			sum += i;
			++found;
		}
	}

	cout << sum << endl;

	return 0;
}
