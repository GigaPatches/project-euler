/**
 * Project 18
 *
 * Find the maximum total from top to bottom of the triangle.
 */

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include <iterator>
#include <sstream>

using namespace std;

vector< vector<int> > triangle;

vector<int> getrow(ifstream& f)
{
	string line;
	getline(f, line);
	stringstream s(line);
	return vector<int>((istream_iterator<int>(s)), istream_iterator<int>());;
}

int main()
{
	ifstream f("prob18.txt");
	if (!f.is_open())
	{
		cout << "Unable to open file" << endl;
		return 1;
	}

	for (int i = 0; i < 15; ++i)
		triangle.push_back(getrow(f));

	f.close();

	// Walk backwards to find the largest path
	for (int i = 14; i > 0; --i)
		for (int j = 0; j < i; ++j)
			triangle[i-1][j] += max(triangle[i][j], triangle[i][j+1]);

	// The largest path sum is now at [0][0]
	cout << triangle[0][0] << endl;
	return 0;
}
