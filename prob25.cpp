/**
 * Problem 25
 *
 * What is the first term in the Fibonacci sequence to contain 1000 digits?
 */

#include <iostream>
#include <algorithm>
#include <gmpxx.h>

using namespace std;

int main()
{
	int c = 2;
	mpz_class num0 = 1, num1 = 1, tmp;

	while (num0.get_str().length() < 1000)
	{
		tmp = num0 + num1;
		num1 = num0;
		num0 = tmp;
		++c;
	}

	cout << c << endl;

	return 0;
}
