/**
 * Problem 14
 *
 * Given the following:
 *   n --> n/2 (n is even)
 *   n --> 3n +2 (n is odd)
 *
 * What starting number under 1 million, produces the longest chain?
 */

#include <iostream>

int chain(int num)
{
	unsigned int n = static_cast<unsigned int>(num);
	int c = 1;
	while (n > 1)
	{
		if ((n % 2) == 0)
			n /= 2;
		else
			n = 3 * n + 1;
		++c;
	}

	return c;
}

int main()
{
	int max_num = 0;
	int max_count = 0;

	int c;
	for (int i = 1; i < 1000000; ++i)
	{
		c = chain(i);
		if (c > max_count)
		{
			max_num = i;
			max_count = c;
		}
	}

	std::cout << "Largest chain: " << max_num << " (" << max_count << ")" << std::endl;

	return 0;
}
