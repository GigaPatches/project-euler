/**
 * Problem 30
 *
 * Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits
 *
 * One-liner python solution I wrote to find the real answer after I made a typo in this C++ version:
 * sum([n for n in xrange(2, 354294) if sum([int(x) ** 5 for x in str(n)]) == n])
 */

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	int t;
	int total = 0, sum;

	// No >= 7 digit number could be the solution, so we check only 6 digits
	// 6 * 9 ^ 5
	for (int n = 2; n < 354294; ++n)
	{
		t = n;
		sum = 0;

		while (t != 0)
		{
			sum += pow(t % 10, 5);
			t /= 10;
		}

		if (sum == n)
			total += n;
	}

	cout << total << endl;
}
