/**
 * Problem 33
 *
 * The fraction ^(49)/_(98) is a curious fraction, as an inexperienced mathematician in attempting to simplify 
 * it may incorrectly believe that ^(49)/_(98) = ^(4)/_(8), which is correct, is obtained by cancelling the 9s.
 *
 * We shall consider fractions like, ^(30)/_(50) = ^(3)/_(5), to be trivial examples.
 *
 * There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing
 * two digits in the numerator and denominator.
 *
 * If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
 */

#include <iostream>

using namespace std;

int main()
{
	float result = 1;

	for (int a = 1; a < 10; ++a)
	{
		for (int b = 1; b < 10; ++b)
		{
			int n = 10 * a * b;
			int d = 9 * a + b;

			if (n % d == 0)
			{
				int c = n / d;
				if ((a / c) == 0)
				{
					result *= (float)c / a;
				}
			}
		}
	}

	cout << result << endl;
	return 0;
}
