/**
 * Problem 32
 *
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; 
 * for example, the 5-digit number, 15234, is 1 through 5 pandigital.
 *
 * The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and 
 * product is 1 through 9 pandigital.
 *
 * Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
 */

#include <iostream>
#include <numeric>
#include <set>

using namespace std;

bool pandigital(int x, int y)
{
	set<int> digits;
	int z = x * y, digit;

	while (x != 0)
	{
		digit = x % 10;
		if (digit == 0 || digits.find(digit) != digits.end())
			return false;
		digits.insert(digit);
		x /= 10;
	}

	while (y != 0)
	{
		digit = y % 10;
		if (digit == 0 || digits.find(digit) != digits.end())
			return false;
		digits.insert(digit);
		y /= 10;
	}

	while (z != 0)
	{
		digit = z % 10;
		if (digit == 0 || digits.find(digit) != digits.end())
			return false;
		digits.insert(digit);
		z /= 10;
	}

	return digits.size() == 9;
}

int main()
{
	set<int> results;

	for (int i = 2; i <= 100; ++i)
		for (int j = 123; j < 100000/i; ++j)
			if (pandigital(i, j))
				results.insert(i * j);

	cout << accumulate(results.begin(), results.end(), 0) << endl;

	return 0;
}
