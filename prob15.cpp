/**
 * Problem 15
 *
 * How many routes are there through a 20x20 grid?
 */

#include <iostream>
#include <vector>
#include <map>
#include <gmpxx.h>

using namespace std;

//typedef unsigned long long uint64;

map<int, mpz_class> factmap;

/**
 * Stores the results of a factorial in a map for fast lookups.
 */
mpz_class factorial(int n)
{
	if (n <= 1)
		return 1;

	if (factmap[n] != 0)
		return factmap[n];

	int i = n;
	mpz_class v = 1;
//	mpz_init_set_ui(v, 1);
//	mpz_init_set_ui(i, n);
	while (i > 0)
	{
		//v *= i;
		v *= i;
		--i;
	}

	factmap[n] = v;
	return v;
}

int main()
{
//	vector<int> col;

/*	for (int x = 0; x < 40; ++x)
	{
		for (int t = 0; t < 40 - x; ++t)
			cout << " ";
		for (int y = 0; y <= x; ++y)
		{
			//col.push_back(factorial(x) / (factorial(y) * factorial(x - y)));
			cout << " " << factorial(x) / (factorial(y) * factorial(x - y));
		}
		cout << endl;
	}
*/

	cout << factorial(40) << endl;
	cout << factorial(20) << endl;
	cout << factorial(40) / (factorial(20) * factorial(40 - 20)) << endl;

	return 0;
}
