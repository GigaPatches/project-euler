/**
 * Problem 46
 *
 * It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.
 *
 * 9 = 7 + 2×1^(2)
 * 15 = 7 + 2×2^(2)
 * 21 = 3 + 2×3^(2)
 * 25 = 7 + 2×3^(2)
 * 27 = 19 + 2×2^(2)
 * 33 = 31 + 2×1^(2)
 *
 * It turns out that the conjecture was false.
 *
 * What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
 */

#include <iostream>
#include "primes.h"
#define ever (;;)

using namespace std;

int main ()
{
	int result = 1, p, k;

	for ever
	{
		result += 2;

		if (is_prime(result))
			continue;

		for (p = 1; p <= result; --p)
		{
			k = result - 2 * (p * p);
			if (k < 0)
				goto end;
			if (is_prime(k))
				break;
		}
	}

end:
	cout << result << endl;

	return 0;
}

