/**
 * Problem 6
 *
 * Find the difference between the sum of the squares of the first one hundred natural numbers 
 * and the square of the sum
 */

#include <iostream>

using std::cout;
using std::endl;

int main()
{
	int sum = 0;
	int sum2 = 0;

	for (int i = 1; i <= 100; ++i)
	{
		sum += i * i;
		sum2 += i;
	}

	sum2 *= sum2;
	int diff = sum2 - sum;

	cout << "(" << sum2 << " - " << sum << ") = " << diff << endl;

	return 0;
}
