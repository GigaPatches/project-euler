/**
 * Problem 17
 *
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many 
 * letters would be used?
 */

#include <iostream>
#include <string>
#include <map>
#include "boost/assign.hpp"

using namespace std;
using namespace boost::assign;

map<int, string> words = map_list_of (0, "zero")
	(1, "one")
	(2, "two")
	(3, "three")
	(4, "four")
	(5, "five")
	(6, "six")
	(7, "seven")
	(8, "eight")
	(9, "nine")
	(10, "ten")
	(11, "eleven")
	(12, "twelve")
	(13, "thirteen")
	(14, "fourteen")
	(15, "fifteen")
	(16, "sixteen")
	(17, "seventeen")
	(18, "eighteen")
	(19, "nineteen")
	(20, "twenty")
	(30, "thirty")
	(40, "forty")
	(50, "fifty")
	(60, "sixty")
	(70, "seventy")
	(80, "eighty")
	(90, "ninety")
	(100, "hundred")
	(1000, "thousand");

string tens(int);
string hundreds(int);
string towords(int);
//string largenum(int);

string tens(int num)
{
	int tens = (num / 10) * 10;
	int units = num % 10;
	string c = words[tens];
	if (units > 0)
		c += "-" + words[units];
	return c;
}

string hundreds(int num)
{
	int hundreds = num / 100;
	int rest = num % 100;
	string c = words[hundreds] + " " + words[100];
	if (rest > 0)
		c += " and " + towords(rest);
	return c;
}

string thousands(int num)
{
	int thousands = num / 1000;
	int rest = num % 1000;
	string c = words[thousands] + " " + words[1000];
	if (rest >= 100)
		c += " and " + towords(rest);
	else if (rest > 0)
		c += ", " + towords(rest);
	return c;
}

string towords(int num)
{
	if (num < 20)
		return words[num];

	if (num < 100)
		return tens(num);

	if (num < 1000)
		return hundreds(num);

	return thousands(num);
}

int count_chars(string s)
{
	int sum = 0;
	char c;
	for (int i = 0; i < s.length(); ++i)
	{
		c = s.at(i);
		if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <='z'))
			sum++;
	}
	return sum;
}

int main()
{
	int sum = 0;
	for (int i = 1; i <= 1000; ++i)
		sum += count_chars(towords(i));

	cout << sum << endl;
	return 0;
}
