/**
 * Problem 39
 *
 * If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly 
 * three solutions for p = 120.
 *
 * {20,48,52}, {24,45,51}, {30,40,50}
 *
 * For which value of p ≤ 1000, is the number of solutions maximised?
 */

#include <iostream>

using namespace std;

int main()
{
	int mc = 0, mn = 0, p, b, a, c, count;

	for (p = 1; p < 1000; ++p)
	{
		count = 0;
		for (b = 1; b < p; ++b)
		{
			a = (p * p - 2 * b * p) / (2 * p - 2 * b);
			c = (p * p - 2 * b * p + 2 * b * b) / (2 * p - 2 * b);
			if ((a % 1 == 0) && (a > 0) && (c % 1 == 0))
				count++;
		}
		if (count > mc)
		{
			mc = count;
			mn = p;
		}
	}

	cout << mc << ' ' << mn << endl;

	return 0;
}
