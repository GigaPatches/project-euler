#pragma once
/**
 * Calculates if a number is pandigital
 */
#include <math.h>

using namespace std;

bool is_pandigital(unsigned long long n)
{
	bool digits[10] = {0};
	int s = (int)ceil(log10(n));
	int tmp;

	while (n != 0)
	{
		tmp = (int)(n % 10);
		if (tmp > s || digits[tmp])
			return false;
		digits[tmp] = true;
		n /= 10;
	}

	if (digits[0])
		return false;

	for (int i = 1; i <= s; ++i)
		if (!digits[i])
			return false;

	return true;
}
