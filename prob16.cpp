/**
 * Problem 16
 *
 * What is the sum of the digits of the number 2^(1000)?
 */

#include <iostream>
#include <gmpxx.h>
#include <gmp.h>

using namespace std;

int main()
{
	string strnum;

	mpz_class num = 2;
	mpz_pow_ui(num.get_mpz_t(), num.get_mpz_t(), 1000);

 	strnum = num.get_str();

	int sum = 0;

	for (int i = 0; i < strnum.length(); ++i)
		sum += static_cast<int>(strnum.at(i) - 48);

	cout << sum << endl;

	return 0;
}
