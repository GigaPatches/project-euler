/**
 * Problem 4
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */

#include <iostream>

using std::cout;
using std::endl;

bool IsPalin(int n)
{
	int r = 0, t = n;
	
	while (t > 0)
	{
		r = (r * 10) + (t % 10);
		t /= 10;
	}

	return n == r;
}

int main()
{
	int largest = 0;

	for (int x = 100; x <= 999; ++x)
		for (int y = 100; y <= 999; ++y)
		{
			int tmp = x * y;
			if (IsPalin(tmp) && largest < tmp)
				largest = tmp;
		}

	cout << "Largest palindrome: " << largest << endl;

	return 0;
}
