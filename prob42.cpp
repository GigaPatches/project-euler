/**
 * Problem 42
 *
 * The n^(th) term of the sequence of triangle numbers is given by, t_(n) = ½n(n+1); so the first ten triangle numbers are:
 *
 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
 *
 * By converting each letter in a word to a number corresponding to its alphabetical position and adding 
 * these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t_(10). If the word value 
 * is a triangle number then we shall call the word a triangle word.
 *
 * Using words.txt, a 16K text file containing nearly two-thousand common English words, how many are triangle words?
 */

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

using namespace std;

int score(string s)
{
	int sum = 0;

	for (string::iterator i = s.begin(); i != s.end(); ++i)
		sum += *i - 64;

	return sum;
}

int t(int n0)
{
	return 0.5 * n0 * (n0 + 1);
}

int n(int t0)
{
	return 0.5 * (sqrt(8 * t0 + 1) - 1);
}

int main()
{
	int count = 0;

	ifstream file("words.txt");
	if (!file.is_open())
	{
		cout << "Unable to open 'names.txt'!" << endl;
		return 1;
	}

	string line;
	while (getline(file, line, ','))
	{
		int s = score(line.substr(1, line.length() - 2));

		if (t(n(s)) == s)
			++count;
	}

	cout << count << endl;

	return 0;
}
