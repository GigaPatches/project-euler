/**
 * Problem 22
 *
 * Using names.txt begin by sorting it into alphabetical order. Then working out the
 * alphabetical value for each name, multiply this value by its alphabetical position
 * in the list to obtain a name score.
 *
 * For example, when the list is sorted into alphabetical order, COLIN, 
 * which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. 
 * So, COLIN would obtain a score of 938 × 53 = 49714.
 *
 * What is the total of all the name scores in the file?
 */

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

int score(string s)
{
	int sum = 0;

	for (string::iterator i = s.begin(); i != s.end(); ++i)
		sum += *i - 64;

	return sum;
}

int main()
{
	ifstream file("names.txt");
	if (!file.is_open())
	{
		cout << "Unable to open 'names.txt'";
		return 1;
	}

	vector<string> names;
	string line;

	while (getline(file, line, ','))
		names.push_back(line.substr(1, line.length() - 2));

	sort(names.begin(), names.end());
	int c = 1, result = 0;
	for (vector<string>::iterator it = names.begin(); it != names.end(); ++it, ++c)
		result += score(*it) * c;

	cout << result << endl;

	return 0;
}
