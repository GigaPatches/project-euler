/**
 * Problem 38
 *
 * Take the number 192 and multiply it by each of 1, 2, and 3:
 *
 *     192 × 1 = 192
 *     192 × 2 = 384
 *     192 × 3 = 576
 *
 * By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 
 * the concatenated product of 192 and (1,2,3)
 *
 * The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 
 * 918273645, which is the concatenated product of 9 and (1,2,3,4,5).
 *
 * What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an 
 * integer with (1,2, ... , n) where n > 1?
 */

#include <iostream>
#include <cmath>
#include "pandigital.h"

using namespace std;

int main()
{
	unsigned long long tmp = 0, max = 0;

	for (int i = 1; i <= 99999; ++i)
	{
		tmp = i;
		int c = 2;

		while (ceil(log10(tmp)) < 9)
		{
			for (int j = 0; j < log10(i * c); ++j)
				tmp *= 10;
			tmp += i * c++;
		}


		if (is_pandigital(tmp) && tmp > max)
			max = tmp;
	}

	cout << max << endl;

	return 0;
}
