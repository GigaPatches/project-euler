/**
 * Problem 40
 *
 * An irrational decimal fraction is created by concatenating the positive integers:
 *
 * 0.123456789101112131415161718192021...
 *
 * It can be seen that the 12^(th) digit of the fractional part is 1.
 *
 * If d_(n) represents the n^(th) digit of the fractional part, find the value of the following expression.
 *
 * d_(1) × d_(10) × d_(100) × d_(1000) × d_(10000) × d_(100000) × d_(1000000)
 */

#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#define MAX_LENGTH 1000000

using namespace std;

int main()
{
	string str;
	ostringstream strstream;
	int written = 0, i = 1, result;

	while (written < MAX_LENGTH)
	{
		strstream << i;
		written += ceil(log10(i));
		++i;
	}

	str = strstream.str();

	result = 1;
	for (i = 1; i < MAX_LENGTH; i *= 10)
		result *= (int)(str[i - 1] - '0');

	cout << result << endl;

	return 0;
}
