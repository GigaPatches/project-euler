/**
 * Problem 13
 *
 * Work out the first ten digits of the sum of the following one-hundred 50-digit numbers. (see prob13.txt)
 *
 * Alternative working:
 * 	Use only the first 11 digits
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <gmpxx.h>

using namespace std;

int main()
{
	ifstream f("prob13.txt");

	if (!f.is_open())
	{
		cout << "Unable to open file." << endl;
		return 1;
	}

	mpz_class sum = 0;
	string tmpstr;
	mpz_class tmp;
	while (getline(f, tmpstr))
	{
		tmp = tmpstr;
		sum += tmp;
	}
	f.close();

	stringstream o;
	o << sum;
	cout << sum << endl;
	cout << o.str().substr(0, 10) << endl;
	
	return 0;
}
