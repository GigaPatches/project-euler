/**
 * Problem 12
 *
 * What is the value of the first triangle number to have over five hundred divisors?
 */

#include <iostream>
#include <math.h>
//#include <vector>

using std::cout;
using std::endl;

int count_divisors(int n)
{
//	vector<int> factors;
	int m = static_cast<int>(sqrt(n));
	int c = 0;

	for (int i = 1; i  < m; ++i)
	{
		if ((n % i) == 0)
			c += 2;
	}

	return c;
}

int triangle(int n)
{
	return ((n + 1) * n) / 2;
	/*int c = 0;
	
	for (int i = 1; i < n; ++i)
		c += i;

	return c;*/
}

int main()
{
	int tri = 0, divisors = 0, i = 0;

	while (divisors <= 500)
	{
		tri += ++i;
		divisors = count_divisors(tri);
	}

	cout << tri << endl;

	return 0;
}
