/**
 * Problem 11
 *
 * What is the greatest product of four adjacent numbers in any direction (up, down, left, right, or diagonally)
 * in the 20×20 grid?
 */

#include <iostream>
#include <fstream>

using namespace std;

int grid[20][20];

int main()
{
	ifstream f("prob11.txt");

	if (!f.is_open())
	{
		cout << "Unable to open file." << endl;
		return 1;
	}

	// Read in data
	for (int x = 0; x < 20; ++x)
		for (int y = 0; y < 20; ++y)
			f >> grid[x][y];


	// Start the scan
	int max = 0;
	int tmp = 0;

	for (int x = 0; x < 20; ++x)
	{
		for (int y = 0; y < 20; ++y)
		{
			if (20 > y + 3)
			{
				tmp = grid[x][y] * grid[x][y + 1] * grid[x][y + 2] * grid[x][y + 3];
				if (max < tmp)
					max = tmp;
			}

			if (20 > x + 3)
			{
			    	tmp = grid[x][y] * grid[x + 1][y] * grid[x + 2][y] * grid[x + 3][y];
				if (max < tmp)
					max = tmp;
			}

			if (20 > x + 3 && 20 > y + 3)
			{
				tmp = grid[x][y] * grid[x + 1][y + 1] * grid[x + 2][y + 2] * grid[x + 3][y + 3];
				if (max < tmp)
					max = tmp;
			}

			if (x + 3 < 20 && y > 3)
			{
				tmp = grid[x][y] * grid[x + 1][y - 1] * grid[x + 2][y - 2] * grid[x + 3][y - 3];
				if (max < tmp)
					max = tmp;
			}
		}
	}

	cout << max << endl;


	return 0;
}
