/**
 * Problem 34
 *
 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
 *
 * Find the sum of all numbers which are equal to the sum of the factorial of their digits.
 */

#include <iostream>
#include <vector>
#include <map>

using namespace std;

map<int, int> fact;

bool checksum(int n)
{
	int t = n, sum = 0;

	while (t != 0)
	{
		sum += fact[t % 10];
		t /= 10;
	}

	return sum == n;
}

int main()
{
	fact[0] = 1;
	for (int i = 1; i < 10; ++i)
		fact[i] = fact[i - 1] * i;

	int sum = 0;

	for (int i = 3; i < 2540160; ++i)
		if (checksum(i))
			sum += i;
	cout << sum << endl;

	return 0;
}
