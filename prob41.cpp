/**
 * Problem 41
 *
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 
 * 2143 is a 4-digit pandigital and is also prime.
 *
 * What is the largest n-digit pandigital prime that exists?
 *
 */

#include <iostream>
#include "primes.h"
#include "pandigital.h"

using namespace std;

int main()
{
	int max = 0;

	for (int i = 1; i <= 9999999; i += 2)
	{
		if (is_pandigital(i) && is_prime(i) && i > max)
			max = i;
	}	

	cout << max << endl;

	return 0;
}
