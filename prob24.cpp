/**
 * Problem 24
 *
 * A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation 
 * of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, 
 * we call it lexicographic order.
 *
 * What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8, and 9?
 */

#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	char nums[] = "0123456789";

	for (int i = 0; i < 1000000; ++i)
		next_permutation(nums, nums + 10);

	cout << nums << endl;

	return 0;
}
