/**
 * Problem 47
 *
 * The first two consecutive numbers to have two distinct prime factors are:
 *
 * 14 = 2 × 7
 * 15 = 3 × 5
 *
 * The first three consecutive numbers to have three distinct prime factors are:
 *
 * 644 = 2² × 7 × 23
 * 645 = 3 × 5 × 43
 * 646 = 2 × 17 × 19.
 *
 * Find the first four consecutive integers to have four distinct primes factors. What is the first of these numbers?
 */

#include <iostream>
#include <map>
#include "primes.h"
#define PRIME_COUNT 10000

using namespace std;

bool primes[PRIME_COUNT] = {false};

int next_prime(int n)
{
	while (++n < PRIME_COUNT && !primes[n]);
	return n;
}

int factors(int n)
{
	if (n < 2)
		return 0;
	int i = 2, c = 0, k = n / 2;

	while (i <= k && i < PRIME_COUNT)
	{
		if ((n % i) == 0 && primes[i])
		{
			++c;
		}

		i = next_prime(i);
	}
		
	return c;
}

int main()
{
	int n = 647, i;

	for (i = 1; i < PRIME_COUNT; ++i)
		primes[i] = is_prime(i);

	while (!(factors(n) == 4 && factors(n + 1) == 4 && factors(n + 2) == 4 && factors(n + 3) == 4))
		++n;

	cout << n << endl;

	return 0;
}
