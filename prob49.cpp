/**
 * Problem 49
 *
 * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: 
 * (i) each of the three terms are prime, and, 
 * (ii) each of the 4-digit numbers are permutations of one another.
 *
 * There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but 
 * there is one other 4-digit increasing sequence.
 *
 * What 12-digit number do you form by concatenating the three terms in this sequence?
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include "primes.h"

using namespace std;

vector<int> to_vector(int n)
{
	vector<int> v;

	while (n != 0)
	{
		v.push_back(n % 10);
		n /= 10;
	}

	return v;
}

bool is_permutation(int n1, int n2)
{
	vector<int> v1 = to_vector(n1), v2 = to_vector(n2);
	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());
	return v1 == v2;
}

int main()
{
	int a, b, c;

	int MAX = 10000 - 3330 * 2;
	for (a = 1000; a <= MAX; ++a)
	{
		b = a + 3330;
		c = b + 3330;

		if (is_prime(a) && is_prime(b) && is_prime(c))
		{
			if (is_permutation(a, b) && is_permutation(b, c))
			{
				cout << a << b << c << endl;
			}
		}
	}

	return 0;
}
