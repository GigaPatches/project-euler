/**
 * Problem 7
 *
 * What is the 10001^(st) prime number?
 */

#include <iostream>
#include "primes.h"

using std::cout;
using std::endl;

int main()
{
	int i = 1;
	int num = 1;
	while (i <= 10001)
	{
		while (!is_prime(++num));
		++i;
	}

	cout << num << endl;
	return 0;
}
