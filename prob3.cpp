/**
 * Problem 3
 *
 * What is the largest prime factor of the number 600851475143 ?
 */

#include <iostream>
#include <math.h>
#include "primes.h"

using namespace std;

int main()
{
	long n = 600851475143;
	int max = 0;

	int sq = ceil(sqrt(static_cast<float>(n)));
	for (long i = 2; i <= sq; ++i)
		if ((n % i) == 0 && is_prime(i))
			if (i > max)
				max = i;

	cout << max << endl;

	return 0;
}
