#include <math.h>

/**
 * Determine if the specified number is a prime or not.
 */
bool is_prime(int n)
{
	if (n <= 1)
		return false;
	if (n < 4)
		return true;
	if ((n % 2) == 0)
		return false;
	if (n < 9)
		return true;
	if ((n % 3) == 0)
		return false;

	int r = floor(sqrt(static_cast<int>(n)));
	for (int f = 5; f <= r; f += 6)
		if ((n % f) == 0 || (n % (f + 2)) == 0)
			return false;

	return true;
}
