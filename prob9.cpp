/**
 * Problem 9
 *
 * There exists exactly one Pythagorean triplet for which a + b  + c = 1000.
 * Find the product abc.
 */

#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

bool IsTriplet(int a, int b, int c)
{
	return (a < b && b < c) &&((a * a) + (b * b)) == (c * c);
}

int main()
{
	int a = 0, b = 0, c = 0;

	for (a = 0; a < 1000; ++a) for (b = 0; b < 1000; ++b) for (c = 0; c < 1000; ++c)
	{
		if ((a + b + c) == 1000 && IsTriplet(a, b, c))
		{
			cout << a * b * c << endl;
			return 0;
		}
	}

	return 0;
}
