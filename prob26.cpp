/**
 * Problem 26
 *
 * Find the value of d < 1000 for which ^(1)/_(d) contains the longest recurring cycle in its decimal fraction part.
 */

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	int maxl = 0, maxn = 0;

	for (int i = 2; i < 1000; ++i)
	{
		int remain = 1, len = 0, rest = 1, r;
		for (int j = 0; j < i; ++j)
			rest = (rest * 10) % i;
		r = rest;
		do
		{
			rest = (rest * 10) % i;
			++len;
		} while (rest != r);

		if (len > maxl)
		{
			maxl = len;
			maxn = i;
		}
	}

	cout << maxl << " " << maxn << endl;

	return 0;
}
