/**
 * Problem 19
 *
 * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */

#include <iostream>

using namespace std;

int monthdays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

bool leapyear(int y)
{
	return (y % 400) == 0 || ((y % 100) != 0 && (y % 4) == 0);
}

int main()
{
	int counter = 0, wday = 0;

	for (int year = 1900; year <= 2000; ++year)
	{
		for (int month = 0; month < 12; ++month)
		{
			int md = monthdays[month] + (month == 1 && leapyear(year));
			for (int day = 0; day < md; ++day)
			{
				if (year > 1900 && day == 0 && wday == 6)
					++counter;

				++wday;
				if (wday > 6)
					wday = 0;
			}
		}
	}

	cout << counter << endl;
	return 0;
}
