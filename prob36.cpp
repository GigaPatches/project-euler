/**
 * The decimal number, 585 = 1001001001_(2) (binary), is palindromic in both bases.
 *
 * Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
 */

#include <iostream>

using namespace std;

unsigned int reverse(unsigned int n, int b)
{
	unsigned int r = 0;

	while (n > 0)
	{
		r = (r * b) + (n % b);
		n /= b;
	}

	return r;
}

int main()
{
	int sum = 0;
	for (unsigned int i = 1; i < 1000000; ++i)
		if (i == reverse(i, 10) && i == reverse(i, 2))
			sum += i;

	cout << sum << endl;

	return 0;
}
