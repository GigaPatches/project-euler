/**
 * Problem 35
 *
 * The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
 *
 * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
 *
 * How many circular primes are there below one million?
 */

#include <iostream>
#include <math.h>
#include <algorithm>
#include "primes.h"

using namespace std;

int rotate(int n)
{
	return n / 10 + (n % 10) * pow(10, ceil(log10(n)) - 1);
}

bool is_circular(int n)
{
	int p  = n;

	do
	{
		p = rotate(p);
		if (!is_prime(p))
			return false;
	} while (p != n);

	return true;
}

int main()
{
	int sum = 1;
	for (int i = 1; i < 1000000; i += 2)
	{	
		if (is_circular(i))
		{
			++sum;
		}
	}
		

	cout << sum  << endl;

	return 0;
}
