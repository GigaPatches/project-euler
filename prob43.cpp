/**
 * Problem 43
 * The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9
 * in some order, but it also has a rather interesting sub-string divisibility property.
 *
 * Let d_(1) be the 1^(st) digit, d_(2) be the 2^(nd) digit, and so on. In this way, we note the following:
 *
 *     * d_(2)d_(3)d_(4)=406 is divisible by 2
 *     * d_(3)d_(4)d_(5)=063 is divisible by 3
 *     * d_(4)d_(5)d_(6)=635 is divisible by 5
 *     * d_(5)d_(6)d_(7)=357 is divisible by 7
 *     * d_(6)d_(7)d_(8)=572 is divisible by 11
 *     * d_(7)d_(8)d_(9)=728 is divisible by 13
 *     * d_(8)d_(9)d_(10)=289 is divisible by 17
 *
 * Find the sum of all 0 to 9 pandigital numbers with this property.
 */

#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>

using namespace std;

int subint(int p, int l, long long i)
{
	int o;
	stringstream s;
	s << i;
	stringstream(s.str().substr(p, l)) >> o;

	cout << p << ' ' << l << ' ' << o << endl;
	return o;
}

int intcat(char a, char b, char c)
{
	return  (a - '0') * 100 + (b - '0') * 10 + (c - '0');
}

int main()
{
	long long sum = 0, x;
	char num[] = "1234567890";

	do
	{
		if (num[0] == '0')
			continue;

		if (intcat(num[1], num[2], num[3]) % 2 == 0 &&
		    intcat(num[2], num[3], num[4]) % 3 == 0 &&
		    intcat(num[3], num[4], num[5]) % 5 == 0 &&
		    intcat(num[4], num[5], num[6]) % 7 == 0 &&
		    intcat(num[5], num[6], num[7]) % 11 == 0 &&
		    intcat(num[6], num[7], num[8]) % 13 == 0 &&
		    intcat(num[7], num[8], num[9]) % 17 == 0)
		{
			stringstream(num) >> x;
			sum += x;
		}
	} while (next_permutation(num, num + 10));

	cout << sum << endl;

	return 0;
}
